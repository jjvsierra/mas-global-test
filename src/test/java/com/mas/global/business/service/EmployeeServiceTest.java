package com.mas.global.business.service;

import com.mas.global.business.domain.ContractTypeHourlyDTO;
import com.mas.global.business.domain.EmployeeDomainDTO;
import com.mas.global.crosscutting.mapper.Mapper;
import com.mas.global.entrypoint.web.EmployeeDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

   @Mock
   private Mapper<EmployeeDTO, EmployeeDomainDTO> employeeDTOMapper;

   @InjectMocks
   private EmployeeService employeeService;

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);
   }

   @Test
   public void getAllEmployeesTest() {
      Mockito.when(employeeDTOMapper.map(Mockito.any(EmployeeDTO.class)))
            .thenReturn(getEmployeeDomainDTO());
      List<EmployeeDomainDTO> employeeDomainDTOS = employeeService.getAllEmployees(getEmployees());
      Assert.assertEquals("1", employeeDomainDTOS.get(0).getId());
      Assert.assertEquals("Peter", employeeDomainDTOS.get(0).getName());
      Assert.assertEquals(BigDecimal.valueOf(86400000) , employeeDomainDTOS.get(1).getContractType().getAnnualSalary());
      Assert.assertEquals("1", employeeDomainDTOS.get(1).getId());
      Assert.assertEquals("Peter", employeeDomainDTOS.get(1).getName());
      Assert.assertEquals(BigDecimal.valueOf(86400000) , employeeDomainDTOS.get(1).getContractType().getAnnualSalary());


   }

   private EmployeeDomainDTO getEmployeeDomainDTO() {
      return EmployeeDomainDTO.builder()
            .id("1")
            .name("Peter")
            .contractType(new ContractTypeHourlyDTO(60000))
            .roleId("1")
            .roleName("Administrator")
            .build();
   }

   private List<EmployeeDTO> getEmployees() {
      ArrayList<EmployeeDTO> employeeDTOS = new ArrayList<>();
      employeeDTOS.add(EmployeeDTO.builder().id("1").name("Peter").contractTypeName("HourlySalaryEmployee")
            .roleId("1").roleName("Administrator").roleDescription(null).hourlySalary(60000).monthlySalary(80000)
            .build());
      employeeDTOS.add(EmployeeDTO.builder().id("2").name("Math").contractTypeName("MonthlySalaryEmployee")
            .roleId("2").roleName("Contractor").roleDescription(null).hourlySalary(60000).monthlySalary(80000)
            .build());
      return employeeDTOS;
   }


}