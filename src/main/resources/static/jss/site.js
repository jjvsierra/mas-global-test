var contentTable = document.querySelector('#contentTable')
const URL_EMPLOYEES = 'http://localhost:8080/api/employees'
const URL_EMPLOYEES_ID = 'http://localhost:8080/api/employees/'

function getEmployees(){
    var textBoxValue = document.getElementById("inlineFormInputGroupUsername2").value;
    contentTable.innerHTML = ''
    console.log(textBoxValue)
    if(textBoxValue == ''){
        fetch(URL_EMPLOYEES)
            .then((resp)=>{ return resp.json() })
    .then((data)=>{ setTable(data)})
    }else {
        var urlEmployeeID = URL_EMPLOYEES_ID.concat(textBoxValue)
        console.log(urlEmployeeID)
        fetch(urlEmployeeID)
            .then((resp)=>{ return resp.json() })
    .then((data)=>{ setTable(data)})
    }
}

function setTable(data) {
    console.log(data)
    if(data.length > 1 ){
        iterateData(data)
    } else (
        printData(data)
    )}

function iterateData(data) {
    for (let value of data) {
        //console.log(value)
        contentTable.innerHTML +=  `
            <tr>
                <th scope="row">${value.id}</th>
                <td>${value.name}</td>
                <td>${value.roleId}</td>
                <td>${value.roleName}</td>
                <td>${value.contractType.contractTypeName}</td>
                <td>$${value.contractType.annualSalary}</td>
            </tr>
            `
    }
}

function printData(data){
    console.log(data)
    contentTable.innerHTML =  `
            <tr>
                <th scope="row">${data.id}</th>
                <td>${data.name}</td>
                <td>${data.roleId}</td>
                <td>${data.roleName}</td>
                <td>${data.contractType.contractTypeName}</td>
                <td>$${data.contractType.annualSalary}</td>
            </tr>
            `
}