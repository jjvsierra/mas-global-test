package com.mas.global.crosscutting.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Getter
@Component
@Scope("singleton")
public class ServiceClient {

   private final String serviceUrl;

   @Autowired
   public ServiceClient(@Value("${mas.global.api.client.service.url}") final String serviceUrl) {
      this.serviceUrl = serviceUrl;
   }

}
