package com.mas.global.crosscutting.mapper;

public interface Mapper<I, O> {
   O map(I input);
}


