package com.mas.global.crosscutting.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Employee(s) not found")
public class EmployeesNotFoundException extends RuntimeException {

   public EmployeesNotFoundException(final String message) {
      super(message);
   }
}
