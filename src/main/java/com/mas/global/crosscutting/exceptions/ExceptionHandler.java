package com.mas.global.crosscutting.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {

   @ResponseStatus(HttpStatus.NOT_FOUND)
   @org.springframework.web.bind.annotation.ExceptionHandler(EmployeesNotFoundException.class)
   public String exceptionHandlerEvent(final EmployeesNotFoundException e) {
      return e.getMessage();

   }
}
