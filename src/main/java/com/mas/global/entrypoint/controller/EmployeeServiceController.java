package com.mas.global.entrypoint.controller;

import com.mas.global.business.data.EmployeeProvider;
import com.mas.global.business.service.EmployeeService;
import com.mas.global.entrypoint.web.EmployeeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class EmployeeServiceController {

   private EmployeeService employeeService;

   private EmployeeProvider employeeProvider;

   @Autowired
   public EmployeeServiceController(final EmployeeService employeeService, final EmployeeProvider employeeProvider) {
      this.employeeService = employeeService;
      this.employeeProvider = employeeProvider;
   }

   @RequestMapping(method = RequestMethod.GET, path = {"/employees", "/employees/{id}"})
   public ResponseEntity getEmployees(@PathVariable(value = "id", required = false) final Optional<String> id) {
      List<EmployeeDTO> employees = this.employeeProvider.getDataFromApiClient();

      if (id.isPresent()) {
         return new ResponseEntity(employeeService.getEmployeeByID(id.get(), employees), HttpStatus.OK);
      } else {
         return new ResponseEntity(employeeService.getAllEmployees(employees), HttpStatus.OK);
      }
   }

}
