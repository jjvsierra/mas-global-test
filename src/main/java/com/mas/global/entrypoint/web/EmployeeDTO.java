package com.mas.global.entrypoint.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO {

   private String id;
   private String name;
   private String contractTypeName;
   private String roleId;
   private String roleName;
   private String roleDescription;
   private int hourlySalary;
   private int monthlySalary;

}

