package com.mas.global.business.mapper;


import com.mas.global.business.domain.ContractFactoryDTO;
import com.mas.global.business.domain.EmployeeDomainDTO;
import com.mas.global.crosscutting.mapper.Mapper;
import com.mas.global.entrypoint.web.EmployeeDTO;
import lombok.AllArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class EmployeeDTOEmployeeDomainDTO implements Mapper<EmployeeDTO, EmployeeDomainDTO> {

   @Setter
   private ContractFactoryDTO contractFactoryDTO;

   @Override
   public EmployeeDomainDTO map(EmployeeDTO input) {
      return EmployeeDomainDTO.builder()
            .id(input.getId())
            .contractType(contractFactoryDTO
                  .getContractType(input.getContractTypeName(), input.getHourlySalary(), input.getMonthlySalary()))
            .name(input.getName())
            .roleId(input.getRoleId())
            .roleName(input.getRoleName())
            .roleDescription(input.getRoleDescription())
            .build();
   }
}

