package com.mas.global.business.service;

import com.mas.global.business.domain.EmployeeDomainDTO;
import com.mas.global.crosscutting.exceptions.EmployeesNotFoundException;
import com.mas.global.crosscutting.mapper.Mapper;
import com.mas.global.entrypoint.web.EmployeeDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmployeeService {

   private  Mapper<EmployeeDTO, EmployeeDomainDTO> employeeDTOMapper;

   public EmployeeDomainDTO getEmployeeByID(final String employeeID, final List<EmployeeDTO> employeeDTOS) {
      Optional<EmployeeDomainDTO> employeeDomainDTO = employeeDTOS.stream()
            .filter(employee -> employee.getId().equalsIgnoreCase(employeeID))
            .findFirst().map(employeeDTOMapper::map);
      return getEmployeeDomainDTO(employeeDomainDTO);
   }

   public List<EmployeeDomainDTO> getAllEmployees(final List<EmployeeDTO> employeeDTOS) {
      return employeeDTOS.stream().map(employeeDTOMapper::map).collect(Collectors.toList());
   }

   private EmployeeDomainDTO getEmployeeDomainDTO(Optional<EmployeeDomainDTO> employeeDomainDTO) {
      if (employeeDomainDTO.isPresent()) {
         return employeeDomainDTO.get();
      } else {
         throw new EmployeesNotFoundException("Employee(s) not found");
      }
   }
}
