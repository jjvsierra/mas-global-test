package com.mas.global.business.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class EmployeeDomainDTO {

   private String id;
   private String name;
   private ContractTypeDTO contractType;
   private String roleId;
   private String roleName;
   private String roleDescription;
}
