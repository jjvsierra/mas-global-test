package com.mas.global.business.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ContractTypeMonthlyDTO extends ContractTypeDTO {

   public ContractTypeMonthlyDTO(final int monthlySalary) {
      setContractTypeName("Monthly Salary Employee");
      setAnnualSalary(BigDecimal.valueOf(120 * monthlySalary * 12));
   }
}
