package com.mas.global.business.domain;

import org.springframework.stereotype.Component;

@Component
public class ContractFactoryDTO {

   public ContractTypeDTO getContractType(final String contractTypeName, final int hourlySalary, final int monthlySalary) {

      if (contractTypeName.equalsIgnoreCase("HourlySalaryEmployee")) {
         return new ContractTypeHourlyDTO(hourlySalary);
      }

      if (contractTypeName.equalsIgnoreCase("MonthlySalaryEmployee")) {
         return new ContractTypeMonthlyDTO(monthlySalary);
      }

      return null;

   }
}
