package com.mas.global.business.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ContractTypeHourlyDTO extends ContractTypeDTO {

   public ContractTypeHourlyDTO(final int hourlySalary) {
      setContractTypeName("Hourly Salary Employee");
      setAnnualSalary(BigDecimal.valueOf(120 * hourlySalary * 12));
   }
}
