package com.mas.global.business.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ContractTypeDTO {

   private String contractTypeName;
   private BigDecimal annualSalary;

}
