package com.mas.global.business.data;

import com.mas.global.crosscutting.utils.ServiceClient;
import com.mas.global.entrypoint.web.EmployeeDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
@AllArgsConstructor
@Getter
public class EmployeeProvider {

   private final RestTemplate restTemplate;

   private final ServiceClient serviceClientInformation;

   public List<EmployeeDTO> getDataFromApiClient() {
      ResponseEntity<EmployeeDTO[]> response = restTemplate
            .getForEntity(serviceClientInformation.getServiceUrl(), EmployeeDTO[].class);
      return Arrays.asList(response.getBody());
   }
}
